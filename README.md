**About project**
--
The project was created for training in working with the Spring Framework, as well as for submitting coursework and diploma work. The developed information system is designed to automate the work of employees of an enterprise selling rolled metal products.

**Functionality**
--
* User registration;
* authorization and verification of JWT funds;
* delineation of user roles;
* changing user data taking into account validation;
* shopping cart;
* payment for goods using the Yookassa online cash register;
* SMTP notifications.

**Stack**
--
Spring Framework, Angualr, postgresql, docker-compose, Yookassa-sdk.

**Demo**
--
https://www.youtube.com/watch?v=yLpEsHUPbGE
--
**О проекте**
--
Проект создавался для обучения работы со Spring Framework, так же для сдачи курсовой и дипломной работы.
Разработанная информационная система предназначена для автоматизации работы сотрудников предприятия по продаже продукции металлопроката.  

**Реализованный функционал**
--
* регистрация пользователей;
* авторизация и верификация средствами JWT;
* разграничение ролей пользователей;
* изменение данных пользователей с учетом валидации;
* реализована корзина товаров;
* оплата товаров средствами онлайн-кассы Yookassa;
* SMTP нотификации.

**Использованный стек**
--
Spring Framework, Angualr, postgresql, docker-compose, Yookassa-sdk.

**Демо**
--
https://www.youtube.com/watch?v=yLpEsHUPbGE
