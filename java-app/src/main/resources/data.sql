-- USERS INITIALIZATION --
INSERT INTO users (user_id, email, first_name, is_active, is_not_locked, join_date, last_login_date, last_name, password, phone_number, role)
VALUES (1, 'avarageUser@gmail.com', 'user', true, true, null, null, 'user', '$2a$10$67fChR3Li6l7pQLP29F.N.4xNR8GfI11JII8qcBPXI5mup3f.qkza', '88005553535', 'ROLE_USER');

INSERT INTO users (user_id, email, first_name, is_active, is_not_locked, join_date, last_login_date, last_name, password, phone_number, role)
VALUES (2, 'manager@gmail.com', 'manager', true, true, null, null, 'manager', '$2a$10$67fChR3Li6l7pQLP29F.N.4xNR8GfI11JII8qcBPXI5mup3f.qkza', '88005553535', 'ROLE_MANAGER');

INSERT INTO users (user_id, email, first_name, is_active, is_not_locked, join_date, last_login_date, last_name, password, phone_number, role)
VALUES (3, 'admin@gmail.com', 'admin', true, true, null, null, 'admin', '$2a$10$67fChR3Li6l7pQLP29F.N.4xNR8GfI11JII8qcBPXI5mup3f.qkza', '88005553535', 'ROLE_ADMIN');

-- PRODUCT-TYPES INITIALIZATION --
insert into product_types (product_type_id, product_type) VALUES (1, 'Трубы круглые ВГП');
insert into product_types (product_type_id, product_type) VALUES (2, 'Трубы круглые');

-- LENGTHS INITIALIZATION --
INSERT INTO lengths (length_id, product_length) VALUES (1, 6000);
INSERT INTO lengths (length_id, product_length) VALUES (2, 8000);
INSERT INTO lengths (length_id, product_length) VALUES (3, 10000);

-- PROFILE-SIZES INITIALIZATION --
INSERT INTO profile_sizes (profile_size_id, size) VALUES (1, 20);
INSERT INTO profile_sizes (profile_size_id, size) VALUES (2, 25);
INSERT INTO profile_sizes (profile_size_id, size) VALUES (3, 30);
INSERT INTO profile_sizes (profile_size_id, size) VALUES (4, 35);

-- SIDE-THICKNESSES INITIALIZATION --
INSERT INTO side_thickness(side_thickness_id, side_thickness) VALUES (1, 2);
INSERT INTO side_thickness(side_thickness_id, side_thickness) VALUES (2, 2.5);
INSERT INTO side_thickness(side_thickness_id, side_thickness) VALUES (3, 3);
INSERT INTO side_thickness(side_thickness_id, side_thickness) VALUES (4, 1);
INSERT INTO side_thickness(side_thickness_id, side_thickness) VALUES (5, 4);

-- STANDARDS INITIALIZATION --
INSERT INTO standards(standard_id, standard) VALUES (1, 'ГОСТ 3262');
INSERT INTO standards(standard_id, standard) VALUES (2, 'ТУ 14-105-737');

-- STEEL-GRADE INITIALIZATION --
INSERT INTO steel_grade(steel_grade_id, steel_grade) VALUES (1, '1пс');
INSERT INTO steel_grade(steel_grade_id, steel_grade) VALUES (2, '2пс');
INSERT INTO steel_grade(steel_grade_id, steel_grade) VALUES (3, '08пс');
INSERT INTO steel_grade(steel_grade_id, steel_grade) VALUES (4, '10');

-- PRODUCTTYPE_LENGTH INITIALIZATION --
INSERT INTO producttype_length(product_type_id, length_id) VALUES (1, 1);
INSERT INTO producttype_length(product_type_id, length_id) VALUES (2, 1);
INSERT INTO producttype_length(product_type_id, length_id) VALUES (1, 2);
INSERT INTO producttype_length(product_type_id, length_id) VALUES (2, 2);
INSERT INTO producttype_length(product_type_id, length_id) VALUES (1, 3);

-- PRODUCTTYPE_PROFILESIZE INITIALIZATION --
INSERT INTO producttype_profilesize(product_type_id, profile_size_id) VALUES (1, 1);
INSERT INTO producttype_profilesize(product_type_id, profile_size_id) VALUES (2, 1);
INSERT INTO producttype_profilesize(product_type_id, profile_size_id) VALUES (1, 2);
INSERT INTO producttype_profilesize(product_type_id, profile_size_id) VALUES (1, 3);
INSERT INTO producttype_profilesize(product_type_id, profile_size_id) VALUES (2, 3);
INSERT INTO producttype_profilesize(product_type_id, profile_size_id) VALUES (1, 4);

-- PRODUCTTYPE_SIDETHICKNESS INITIALIZATION --
INSERT INTO producttype_sidethickness(product_type_id, side_thickness_id) VALUES (1, 1);
INSERT INTO producttype_sidethickness(product_type_id, side_thickness_id) VALUES (2, 1);
INSERT INTO producttype_sidethickness(product_type_id, side_thickness_id) VALUES (2, 2);
INSERT INTO producttype_sidethickness(product_type_id, side_thickness_id) VALUES (1, 3);
INSERT INTO producttype_sidethickness(product_type_id, side_thickness_id) VALUES (2, 3);

-- PRODUCTTYPE_STANDARD INITIALIZATION --
INSERT INTO producttype_standard(product_type_id, standard_id) VALUES (1, 1);
INSERT INTO producttype_standard(product_type_id, standard_id) VALUES (2, 2);

-- PRODUCTTYPE_STEELGRADE INITIALIZATION --
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (1, 1);
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (2, 1);
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (1, 2);
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (2, 2);
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (1, 3);
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (2, 3);
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (1, 4);
INSERT INTO producttype_steelgrade(product_type_id, steel_grade_id) VALUES (2, 4);

-- PRODUCTS INITIALIZATION --
INSERT INTO products(product_id, price, quantity, product_length_id, product_type_id, profile_size_id, side_thickness_id, standard_id, steel_grade_id)
VALUES (1, 800, 1000, 2, 1, 1, 1, 1, 4);
INSERT INTO products(product_id, price, quantity, product_length_id, product_type_id, profile_size_id, side_thickness_id, standard_id, steel_grade_id)
VALUES (2, 500, 1000, 2, 2, 1, 2, 2, 1);
INSERT INTO products(product_id, price, quantity, product_length_id, product_type_id, profile_size_id, side_thickness_id, standard_id, steel_grade_id)
VALUES (3, 800, 1000, 3, 1, 4, 1, 1, 3);
INSERT INTO products(product_id, price, quantity, product_length_id, product_type_id, profile_size_id, side_thickness_id, standard_id, steel_grade_id)
VALUES (4, 800, 1000, 1, 2, 3, 2, 2, 3);




