package com.kursovaya.service;

import com.kursovaya.entity.Order;
import me.dynomake.yookassa.Yookassa;
import me.dynomake.yookassa.model.Amount;
import me.dynomake.yookassa.model.Payment;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PaymentService {
    Yookassa yookassa = Yookassa.initialize(294086, "test_bAG0rx1bQAHMbVraHwQUPgRV4pfpXAHNyTqYOyRqYLY");

    public Payment createOrderPayment(Order order)  {
        int orderPrice = order.getOrderDetails().stream().mapToInt(o -> (int) (o.getProduct().getPrice() * o.getQuantity())).sum();
        try {
            return yookassa.createPayment(new Amount(BigDecimal.valueOf(orderPrice), "RUB"),
                    "Номер заказа " + order.getId().toString(), "http://localhost:4200/user/management");
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
