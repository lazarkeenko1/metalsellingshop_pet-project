package com.kursovaya.service;

import com.kursovaya.entity.Order;
import com.kursovaya.entity.order.OrderDetails;
import com.sun.mail.smtp.SMTPTransport;
import org.springframework.stereotype.Service;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import static com.kursovaya.constant.EmailConstant.*;
import static javax.mail.Message.RecipientType.TO;

@Service
public class EmailNotificationService {

    public void sendEmail(String subject, String massageText, String email) throws MessagingException {
        Message message = createMessage(subject, massageText, email);
        SMTPTransport smtpTransport = (SMTPTransport) getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL);
        smtpTransport.connect(SMTP_SERVER, USERNAME, PASSWORD);
        smtpTransport.sendMessage(message, message.getAllRecipients());
        smtpTransport.close();
    }

    private Message createMessage(String subject, String massageText, String email) throws MessagingException {
        Message message = new MimeMessage(getEmailSession());
        message.setRecipients(TO, InternetAddress.parse(email, false));
        message.setSubject(subject);
        message.setText(massageText);
        message.saveChanges();
        return message;
    }

    private Session getEmailSession() {
        return Session.getInstance(new Properties());
    }

    public String createTextToPaymentMessage(Order order, String paymentURL){
        StringBuilder text = new StringBuilder();
        text.append("Ваш заказ:\n");
        for (OrderDetails details :order.getOrderDetails()) {
            text.append(details.getProduct().getProductType().getProductType()).append("\t")
                    .append("количество - ").append(details.getQuantity()).append("\n");
        }
        text.append("\nСсылка на оплату: ").append(paymentURL);
        return text.toString();
    }
}